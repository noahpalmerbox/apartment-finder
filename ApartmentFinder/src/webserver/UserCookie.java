package webserver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import database.Database;

public class UserCookie {
	
	private static List<UserCookie> cookies = new ArrayList<UserCookie>();
	
	private int userID;
	private String cookie;
	private boolean isAdmin;
	
	public UserCookie(int userID)
	{
		this.userID = userID;
		generateCookie();
		try {
			ResultSet rs = Database.query("SELECT USER.IsAdmin FROM USER WHERE USER.userID = " + userID + ";");
			rs.next();
			isAdmin  = rs.getBoolean("IsAdmin");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cookies.add(this);
		
	}
	
	
	public static void delete(int userID)
	{
		int index = -1;
		for(UserCookie uc : cookies)
		{
			if(userID == uc.userID)
				index = cookies.indexOf(uc);
				break;
		}
		
		if(index != -1)
		{
			cookies.remove(index);
		}
		
	}
	
	public static void delete(String cookie)
	{
		int index = -1;
		for(UserCookie uc : cookies)
		{
			if(cookie.equals(uc.cookie))
				index = cookies.indexOf(uc);
				break;
		}
		
		if(index != -1)
		{
			cookies.remove(index);
		}
		
		
		
	}
	
	
	
	public static UserCookie getCookie(int userID)
	{
		for(UserCookie uc : cookies)
		{
			if(userID == uc.userID)
				return uc;
		}
		
		System.out.println("Cookie for " + userID + " Missing!");
		return null;
	}

	public static UserCookie getCookie(String cookie)
	{
		for(UserCookie uc : cookies)
		{
			if(cookie.equals(uc.cookie))
				return uc;
		}
		
		System.out.println("Cookie for " + cookie + " Missing!");
		return null;
	}
	
	
	
	
	public boolean isAdmin()
	{
		return isAdmin;
	}
	public String getCookie()
	{
		return cookie;
	}
	
	public int getUserID() {
		return userID;
	}
	
	private void generateCookie()
	{
		String validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder sb = new StringBuilder();
		Random rand = new Random();
		for(int i = 0;i < 10;i++)
		{
			sb.append(rand.nextInt(validChars.length()));
		}
		cookie = sb.toString();
	}
	
	
}
