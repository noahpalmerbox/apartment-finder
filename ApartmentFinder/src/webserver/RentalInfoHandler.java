package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class RentalInfoHandler extends RequestHandler {

	public RentalInfoHandler() {
		super("/rental", "GET");

	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		try {
			Map<String, String> values = parseGetParams(params);
			UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
			String s = values.get("id");
			if(s.equals("userlist"))
			{
				JSONArray current = new JSONArray();
				ResultSet rs = Database.query("SELECT DISTINCT RENTAL.RentalID,RequestDecision,CancelDate,APARTMENT.ApartmentID, State, City, StreetName, StreetNumber, IsHouse, PurchaseDate, StartDate, EndDate, Status, ApartmentNumber, MonthRent, Deposit, SquareFeet, Bedrooms, Bathrooms FROM RENTAL INNER JOIN APARTMENT ON RENTAL.ApartmentID = APARTMENT.ApartmentID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN COMPLEX ON APARTMENT.ComplexID = COMPLEX.ComplexID WHERE RENTAL_USERS.UserID="+uc.getUserID()+" AND CURDATE() BETWEEN StartDate AND EndDate;");
				while(rs.next())
				{
					JSONObject o = new JSONObject();
					int rentid = rs.getInt("RentalID");
					o.put("rentalid", rs.getInt("RentalID"));
					o.put("apartmentid", rs.getInt("ApartmentID"));
					o.put("purchasedate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("PurchaseDate")));
					o.put("startdate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("StartDate")));
					o.put("enddate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("EndDate")));
					boolean cancelRequest = rs.getDate("CancelDate") != null;
					boolean cancelResponse = rs.getBoolean("RequestDecision");
					boolean wasCancelResponseNull = rs.wasNull();
					boolean isAccepted = rs.getBoolean("Status");
					boolean wasRequestNull = rs.wasNull();
					if(cancelRequest && wasCancelResponseNull)
					{
						o.put("status", "Cancel Pending");
					}
					else if(cancelRequest && !wasCancelResponseNull)
					{
						o.put("status", (cancelResponse? "Cancellation Accepted": "Cancellation Rejected"));
					}
						else if(wasRequestNull)
						{
							o.put("status", "Rental Pending");
						}
					else
						o.put("status", (isAccepted? "Rental Accepted": "Rental Rejected"));
					JSONArray renters = new JSONArray();
					ResultSet rentersSet = Database.query("SELECT USER.UserID, FirstName, LastName, Email FROM RENTAL_USERS INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE RentalId="+rentid+"");
					while(rentersSet.next())
					{
						JSONObject renter = new JSONObject();
						renter.put("id", rentersSet.getInt("UserID"));
						renter.put("firstname", rentersSet.getString("FirstName"));
						renter.put("lastname", rentersSet.getString("LastName"));
						renter.put("email", rentersSet.getString("Email"));
						renters.put(renter);
					}
					o.put("renters", renters);
				
					current.put(o);
					
			}
				JSONArray previous = new JSONArray();
				 rs = Database.query("SELECT DISTINCT RENTAL.RentalID, APARTMENT.ApartmentID, State, Status, City, StreetName, StreetNumber, IsHouse, PurchaseDate, StartDate, EndDate, ApartmentNumber, MonthRent, Deposit, SquareFeet, Bedrooms, Bathrooms FROM RENTAL INNER JOIN APARTMENT ON RENTAL.ApartmentID = APARTMENT.ApartmentID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN COMPLEX ON APARTMENT.ComplexID = COMPLEX.ComplexID WHERE RENTAL_USERS.UserID="+uc.getUserID()+" AND EndDate BETWEEN StartDate AND CURDATE();");
				while(rs.next())
				{
					JSONObject o = new JSONObject();
					int rentid = rs.getInt("RentalID");
					o.put("rentalid", rs.getInt("RentalID"));
					o.put("apartmentid", rs.getInt("ApartmentID"));
					o.put("purchasedate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("PurchaseDate")));
					o.put("startdate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("StartDate")));
					o.put("enddate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("EndDate")));
					boolean isAccepted = rs.getBoolean("Status");
					if(rs.wasNull())
						o.put("status", "pending");
					else
						o.put("status", (isAccepted? "accepted": "rejected"));
					JSONArray renters = new JSONArray();
					ResultSet rentersSet = Database.query("SELECT USER.UserID, FirstName, LastName, Email FROM RENTAL_USERS INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE RentalId="+rentid+"");
					while(rentersSet.next())
					{
						JSONObject renter = new JSONObject();
						renter.put("id", rentersSet.getInt("UserID"));
						renter.put("firstname", rentersSet.getString("FirstName"));
						renter.put("lastname", rentersSet.getString("LastName"));
						renter.put("email", rentersSet.getString("Email"));
						renters.put(renter);
					}
					o.put("renters", renters);
				
					previous.put(o);
					
			}
				JSONArray future = new JSONArray();
				 rs = Database.query("SELECT DISTINCT RENTAL.RentalID, APARTMENT.ApartmentID, State, Status, City, StreetName, StreetNumber, IsHouse, PurchaseDate, StartDate, EndDate, ApartmentNumber, MonthRent, Deposit, SquareFeet, Bedrooms, Bathrooms FROM RENTAL INNER JOIN APARTMENT ON RENTAL.ApartmentID = APARTMENT.ApartmentID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN COMPLEX ON APARTMENT.ComplexID = COMPLEX.ComplexID WHERE RENTAL_USERS.UserID="+uc.getUserID()+" AND StartDate BETWEEN CURDATE() AND EndDate;");
				while(rs.next())
				{
					JSONObject o = new JSONObject();
					int rentid = rs.getInt("RentalID");
					o.put("rentalid", rs.getInt("RentalID"));
					o.put("apartmentid", rs.getInt("ApartmentID"));
					o.put("purchasedate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("PurchaseDate")));
					o.put("startdate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("StartDate")));
					o.put("enddate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("EndDate")));
					boolean isAccepted = rs.getBoolean("Status");
					if(rs.wasNull())
						o.put("status", "pending");
					else
						o.put("status", (isAccepted? "accepted": "rejected"));
					JSONArray renters = new JSONArray();
					ResultSet rentersSet = Database.query("SELECT USER.UserID, FirstName, LastName, Email FROM RENTAL_USERS INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE RentalId="+rentid+"");
					while(rentersSet.next())
					{
						JSONObject renter = new JSONObject();
						renter.put("id", rentersSet.getInt("UserID"));
						renter.put("firstname", rentersSet.getString("FirstName"));
						renter.put("lastname", rentersSet.getString("LastName"));
						renter.put("email", rentersSet.getString("Email"));
						renters.put(renter);
					}
					o.put("renters", renters);
				
					future.put(o);
					
			}
				
				JSONObject ret = new JSONObject();
				ret.put("current", current);
				ret.put("past", previous);
				ret.put("future", future);
				OutputStream outputStream = he.getResponseBody();
				he.getResponseHeaders().add("content-type", "application/json");
				he.sendResponseHeaders(200, 0);
				outputStream.write(ret.toString().getBytes());
				outputStream.flush();
				outputStream.close();
				return true;
			
			
			}else if(s.equals("adminlist")) {
				if(!uc.isAdmin())
				{
					loadErrorPage(403, he);
					return false;
				}
				int userId = Integer.parseInt(values.get("userid"));
				JSONArray current = new JSONArray();
				ResultSet rs = Database.query("SELECT DISTINCT RENTAL.RentalID, APARTMENT.ApartmentID, State, City, StreetName, StreetNumber, IsHouse, PurchaseDate, StartDate, EndDate, Status, ApartmentNumber, MonthRent, Deposit, SquareFeet, Bedrooms, Bathrooms FROM RENTAL INNER JOIN APARTMENT ON RENTAL.ApartmentID = APARTMENT.ApartmentID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN COMPLEX ON APARTMENT.ComplexID = COMPLEX.ComplexID WHERE RENTAL_USERS.UserID="+userId+" AND CURDATE() BETWEEN StartDate AND EndDate;");
				while(rs.next())
				{
					JSONObject o = new JSONObject();
					int rentid = rs.getInt("RentalID");
					o.put("rentalid", rs.getInt("RentalID"));
					o.put("apartmentid", rs.getInt("ApartmentID"));
					o.put("purchasedate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("PurchaseDate")));
					o.put("startdate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("StartDate")));
					o.put("enddate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("EndDate")));
					boolean isAccepted = rs.getBoolean("Status");
					if(rs.wasNull())
						o.put("status", "pending");
					else
						o.put("status", (isAccepted? "accepted": "rejected"));
					JSONArray renters = new JSONArray();
					ResultSet rentersSet = Database.query("SELECT USER.UserID, FirstName, LastName, Email FROM RENTAL_USERS INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE RentalId="+rentid+"");
					while(rentersSet.next())
					{
						JSONObject renter = new JSONObject();
						renter.put("id", rentersSet.getInt("UserID"));
						renter.put("firstname", rentersSet.getString("FirstName"));
						renter.put("lastname", rentersSet.getString("LastName"));
						renter.put("email", rentersSet.getString("Email"));
						renters.put(renter);
					}
					o.put("renters", renters);
				
					current.put(o);
					
			}
				JSONArray previous = new JSONArray();
				 rs = Database.query("SELECT DISTINCT RENTAL.RentalID, APARTMENT.ApartmentID, State, Status, City, StreetName, StreetNumber, IsHouse, PurchaseDate, StartDate, EndDate, ApartmentNumber, MonthRent, Deposit, SquareFeet, Bedrooms, Bathrooms FROM RENTAL INNER JOIN APARTMENT ON RENTAL.ApartmentID = APARTMENT.ApartmentID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN COMPLEX ON APARTMENT.ComplexID = COMPLEX.ComplexID WHERE RENTAL_USERS.UserID="+userId+" AND EndDate BETWEEN StartDate AND CURDATE();");
				while(rs.next())
				{
					JSONObject o = new JSONObject();
					int rentid = rs.getInt("RentalID");
					o.put("rentalid", rs.getInt("RentalID"));
					o.put("apartmentid", rs.getInt("ApartmentID"));
					o.put("purchasedate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("PurchaseDate")));
					o.put("startdate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("StartDate")));
					o.put("enddate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("EndDate")));
					boolean isAccepted = rs.getBoolean("Status");
					if(rs.wasNull())
						o.put("status", "pending");
					else
						o.put("status", (isAccepted? "accepted": "rejected"));
					JSONArray renters = new JSONArray();
					ResultSet rentersSet = Database.query("SELECT USER.UserID, FirstName, LastName, Email FROM RENTAL_USERS INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE RentalId="+rentid+"");
					while(rentersSet.next())
					{
						JSONObject renter = new JSONObject();
						renter.put("id", rentersSet.getInt("UserID"));
						renter.put("firstname", rentersSet.getString("FirstName"));
						renter.put("lastname", rentersSet.getString("LastName"));
						renter.put("email", rentersSet.getString("Email"));
						renters.put(renter);
					}
					o.put("renters", renters);
				
					previous.put(o);
					
			}
				JSONArray future = new JSONArray();
				 rs = Database.query("SELECT DISTINCT RENTAL.RentalID, APARTMENT.ApartmentID, State, Status, City, StreetName, StreetNumber, IsHouse, PurchaseDate, StartDate, EndDate, ApartmentNumber, MonthRent, Deposit, SquareFeet, Bedrooms, Bathrooms FROM RENTAL INNER JOIN APARTMENT ON RENTAL.ApartmentID = APARTMENT.ApartmentID INNER JOIN RENTAL_USERS ON RENTAL.RentalID = RENTAL_USERS.RentalID INNER JOIN COMPLEX ON APARTMENT.ComplexID = COMPLEX.ComplexID WHERE RENTAL_USERS.UserID="+userId+" AND StartDate BETWEEN CURDATE() AND EndDate;");
				while(rs.next())
				{
					JSONObject o = new JSONObject();
					int rentid = rs.getInt("RentalID");
					o.put("rentalid", rs.getInt("RentalID"));
					o.put("apartmentid", rs.getInt("ApartmentID"));
					o.put("purchasedate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("PurchaseDate")));
					o.put("startdate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("StartDate")));
					o.put("enddate", new SimpleDateFormat("yyyy-MM-dd").format( rs.getDate("EndDate")));
					boolean isAccepted = rs.getBoolean("Status");
					if(rs.wasNull())
						o.put("status", "pending");
					else
						o.put("status", (isAccepted? "accepted": "rejected"));
					JSONArray renters = new JSONArray();
					ResultSet rentersSet = Database.query("SELECT USER.UserID, FirstName, LastName, Email FROM RENTAL_USERS INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID WHERE RentalId="+rentid+"");
					while(rentersSet.next())
					{
						JSONObject renter = new JSONObject();
						renter.put("id", rentersSet.getInt("UserID"));
						renter.put("firstname", rentersSet.getString("FirstName"));
						renter.put("lastname", rentersSet.getString("LastName"));
						renter.put("email", rentersSet.getString("Email"));
						renters.put(renter);
					}
					o.put("renters", renters);
				
					future.put(o);
					
			}
				
				JSONObject ret = new JSONObject();
				ret.put("current", current);
				ret.put("past", previous);
				ret.put("future", future);
				OutputStream outputStream = he.getResponseBody();
				he.getResponseHeaders().add("content-type", "application/json");
				he.sendResponseHeaders(200, 0);
				outputStream.write(ret.toString().getBytes());
				outputStream.flush();
				outputStream.close();
				return true;
			}
			else {
			int rentalID = Integer.parseInt(values.get("id"));
			ResultSet rs;
			
			if (canUserAccessResource(he,  Database.query("SELECT UserID FROM RENTAL_USERS WHERE RentalID = " + rentalID + ";"))) {
				JSONObject o = new JSONObject();
				rs = Database.query("SELECT * FROM RENTAL INNER JOIN APARTMENT ON RENTAL.ApartmentID = APARTMENT.ApartmentID INNER JOIN COMPLEX ON APARTMENT.ComplexID = COMPLEX.ComplexID WHERE RentalID=" + rentalID + " ;");
				if(!rs.next())
				{
					loadErrorPage(404, he);
					return false;
				}
				o.put("id", rentalID);
				o.put("apartmentid", rs.getInt("ApartmentID"));
				o.put("purchasedate", new SimpleDateFormat("yyyy-MM-dd").format(rs.getDate("PurchaseDate")));
				o.put("startdate", new SimpleDateFormat("yyyy-MM-dd").format(rs.getDate("StartDate")));
				o.put("enddate", new SimpleDateFormat("yyyy-MM-dd").format(rs.getDate("EndDate")));
				rs.getBoolean("Status");
				boolean cancelRequest = rs.getDate("CancelDate") != null;
				boolean cancelResponse = rs.getBoolean("RequestDecision");
				boolean wasCancelResponseNull = rs.wasNull();
				boolean isAccepted = rs.getBoolean("Status");
				boolean wasRequestNull = rs.wasNull();
				if(cancelRequest && wasCancelResponseNull)
				{
					o.put("status", "Cancel Pending");
				}
				else if(cancelRequest && !wasCancelResponseNull)
				{
					o.put("status", (cancelResponse? "Cancellation Accepted": "Cancellation Rejected"));
				}
					else if(wasRequestNull)
					{
						o.put("status", "Rental Pending");
					}
				else
					o.put("status", (isAccepted? "Rental Accepted": "Rental Rejected"));
				if (rs.getString("StatusReason") != null) {
					o.put("reason", rs.getString("StatusReason"));
				}
				else {
					o.put("reason", "None");
				}
				StringBuilder sb = new StringBuilder();
				sb.append(rs.getInt("StreetNumber"));
				sb.append(" ");
				sb.append(rs.getString("StreetName"));
				if(!rs.getBoolean("IsHouse")) {
					sb.append(" Apartment ");
					sb.append(rs.getInt("ApartmentNumber"));
					}
				sb.append(", ");
				sb.append(rs.getString("City"));
				sb.append(", ");
				sb.append(rs.getString("State"));
				o.put("address", sb.toString());
				JSONArray a = new JSONArray();
						rs = Database.query("SELECT USER.UserID, FirstName, LastName, Email FROM RENTAL_USERS INNER JOIN USER ON RENTAL_USERS.UserID = USER.UserID  WHERE RentalID = " + rentalID + ";");
						while(rs.next())
						{
							JSONObject user = new JSONObject();
							user.put("id", rs.getInt("UserID"));
							user.put("firstname", rs.getString("FirstName"));
							user.put("lastname", rs.getString("LastName"));
							user.put("email", rs.getString("Email"));
							a.put(user);
						}
						
						o.put("renters", a);
						
						OutputStream outputStream = he.getResponseBody();
						he.getResponseHeaders().add("content-type", "application/json");
						he.sendResponseHeaders(200, 0);
						outputStream.write(o.toString().getBytes());
						outputStream.flush();
						outputStream.close();
						return true;
				
			} else {
				loadErrorPage(403, he);
				return false;
			}
			
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}

		
	}

}
