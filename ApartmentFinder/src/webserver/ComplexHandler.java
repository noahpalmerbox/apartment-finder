package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class ComplexHandler extends RequestHandler {

	public ComplexHandler() {
		super("/complex", "GET");
		
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
	
		try {
			Map<String, String> values = parseGetParams(params);
			String s = values.get("id");
			if(s.equals("list"))
			{
				ResultSet rs = Database.query(
						"SELECT * FROM COMPLEX;");
				JSONArray a = new JSONArray();
				while(rs.next())
				{
					JSONObject o = new JSONObject();
					o.put("id", rs.getInt("ComplexID"));
					o.put("isclosetocampus", rs.getBoolean("IsCloseToCampus"));
					o.put("petsallowed", rs.getBoolean("petsAllowed"));
					o.put("isHouse", rs.getBoolean("isHouse"));
					StringBuilder sb = new StringBuilder();
					sb.append(rs.getInt("StreetNumber"));
					sb.append(" ");
					sb.append(rs.getString("StreetName"));
					sb.append(", ");
					sb.append(rs.getString("City"));
					sb.append(", ");
					sb.append(rs.getString("State"));
					o.put("address", sb.toString());
					a.put(o);
				}
			
				OutputStream outputStream = he.getResponseBody();
				he.getResponseHeaders().add("content-type", "application/json");
				he.sendResponseHeaders(200, 0);
				outputStream.write(a.toString().getBytes());
				outputStream.flush();
				outputStream.close();
				he.close();
				return true;
			
			}
			else {
			int id = Integer.parseInt(values.get("id"));
			ResultSet rs = Database.query(
					"SELECT * FROM COMPLEX WHERE ComplexID="+id+";");
			if (!rs.next()) {
				loadErrorPage(404, he);
				return false;
			}
		JSONObject o = new JSONObject();
		o.put("id", id);
		o.put("isclosetocampus", rs.getBoolean("IsCloseToCampus"));
		o.put("petsallowed", rs.getBoolean("petsAllowed"));
		o.put("isHouse", rs.getBoolean("isHouse"));
		StringBuilder sb = new StringBuilder();
		sb.append(rs.getInt("StreetNumber"));
		sb.append(" ");
		sb.append(rs.getString("StreetName"));
		sb.append(" ");
		sb.append(rs.getString("City"));
		sb.append(", ");
		sb.append(rs.getString("State"));
		o.put("address", sb.toString());
		OutputStream outputStream = he.getResponseBody();
		he.getResponseHeaders().add("content-type", "application/json");
		he.sendResponseHeaders(200, 0);
		outputStream.write(o.toString().getBytes());
		outputStream.flush();
		outputStream.close();
		he.close();
		return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}
		
	}

}
