package webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public abstract class RequestHandler {
	private static List<RequestHandler> postHandlers = new ArrayList<RequestHandler>();
	private static List<RequestHandler> getHandlers = new ArrayList<RequestHandler>();

	private String uri;

	public RequestHandler(String uri, String requestType) {
		this.uri = uri;
		if (requestType.equals("GET")) {
			getHandlers.add(this);
		} else if (requestType.equals("POST")) {
			postHandlers.add(this);
		} else {
			System.err.println("RequestHandler: Invalid Request type");
		}

	}

	static boolean canUserAccessResource(HttpExchange he, ResultSet validusers) throws SQLException {
		UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
		if (uc.isAdmin()) {
			return true;
		}
		while (validusers.next())
			if (uc.getUserID() == validusers.getInt("UserID")) {
				return true;
			}

		return false;

	}

	static boolean redirect(HttpExchange he, String newUri) {
		he.getResponseHeaders().add("Location", newUri);
		try {
			he.sendResponseHeaders(302, 0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}
		he.close();
		return true;
	}

	static void loadErrorPage(int code, HttpExchange he) {
		String page = "";
		switch (code) {
		case 400:
			page = "/html/400.html";
			break;
		case 404:
			page = "/html/404.html";
			break;
		case 403:
			page = "/html/403.html";
			break;
		case 500:
			page = "/html/500.html";
			break;
		default:
			System.out.println("Error " + code + " not implemented");
			return;
		}

		OutputStream outputStream = he.getResponseBody();
		StringBuilder htmlBuilder = new StringBuilder();
		InputStream f = RequestHandler.class.getResourceAsStream(page);

		try {
			he.sendResponseHeaders(code, htmlBuilder.toString().length());
			outputStream.write(
					new BufferedReader(new InputStreamReader(f)).lines().collect(Collectors.joining("\n")).getBytes());
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		he.close();
	}

	static Map<String, String> parsePostBody(InputStream is) {

		// turn post body into string
		StringBuilder sb = new StringBuilder();
		try {
			while (is.available() > 0) {
				sb.append((char) is.read());
			}
		} catch (IOException e) {

			e.printStackTrace();
			return null;
		}

		// remove escape characters
		char c;
		int escapeIndex = 0;
		while (sb.indexOf("%", escapeIndex) != -1) {
			c = (char) Integer
					.parseInt(sb.substring(sb.indexOf("%", escapeIndex) + 1, sb.indexOf("%", escapeIndex) + 3), 16);
			escapeIndex = sb.indexOf("%");
			sb.replace(sb.indexOf("%", escapeIndex), sb.indexOf("%", escapeIndex) + 3, Character.toString(c));
			escapeIndex++;
		}

		while (sb.indexOf("+") != -1) {
			sb.replace(sb.indexOf("+"), sb.indexOf("+") + 1, " ");

		}
		// System.out.println(sb);
		// create map
		Map<String, String> returnMap = new HashMap<String, String>();
		int index = 0;
		while (sb.length() > 0) {

			String key;
			String value;
			key = sb.substring(0, sb.indexOf("="));
			sb.replace(0, sb.indexOf("=") + 1, "");

			value = sb.substring(0, (sb.indexOf("&") == -1 ? sb.length() : sb.indexOf("&")));
			sb.replace(0, (sb.indexOf("&") == -1 ? sb.length() : sb.indexOf("&") + 1), "");
			returnMap.put(key, value);

		}
		return returnMap;

	}

	public abstract boolean handle(HttpExchange he, String params);

	public static boolean handlePostRequest(HttpExchange he, String uri, String params) {
		for (RequestHandler rh : postHandlers) {
			if (rh.uri.equals(uri)) {
				Database.startTransaction();
				if (rh.handle(he, params))
					Database.commitTransaction();
				else
					Database.rollbackTransaction();

				return true;
			}
		}
		return false;
	}

	static Map<String, String> parseGetParams(String s) {

		// turn post body into string
		StringBuilder sb = new StringBuilder();
		sb.append(s);
		// remove escape characters
		char c;
		int escapeIndex = 0;
		while (sb.indexOf("%", escapeIndex) != -1) {
			c = (char) Integer
					.parseInt(sb.substring(sb.indexOf("%", escapeIndex) + 1, sb.indexOf("%", escapeIndex) + 3), 16);
			escapeIndex = sb.indexOf("%");
			sb.replace(sb.indexOf("%", escapeIndex), sb.indexOf("%", escapeIndex) + 3, Character.toString(c));
			escapeIndex++;
		}

		// System.out.println(sb);
		// create map
		Map<String, String> returnMap = new HashMap<String, String>();
		int index = 0;
		while (sb.length() > 0) {

			String key;
			String value;
			key = sb.substring(0, sb.indexOf("="));
			sb.replace(0, sb.indexOf("=") + 1, "");

			value = sb.substring(0, (sb.indexOf("&") == -1 ? sb.length() : sb.indexOf("&")));
			sb.replace(0, (sb.indexOf("&") == -1 ? sb.length() : sb.indexOf("&") + 1), "");
			returnMap.put(key, value);

		}
		return returnMap;

	}

	public static boolean handleGetRequest(HttpExchange he, String uri, String params) {

		for (RequestHandler rh : getHandlers) {
			if (rh.uri.equals(uri)) {
				Database.startTransaction();
				if (rh.handle(he, params))
					Database.commitTransaction();
				else
					Database.rollbackTransaction();
				return true;
			}
		}
		return false;
	}

	static void copy(InputStream in, OutputStream out) throws IOException {
		byte[] buf = new byte[8192];
		int length;
		while ((length = in.read(buf)) > 0) {
			out.write(buf, 0, length);
		}
	}

}
