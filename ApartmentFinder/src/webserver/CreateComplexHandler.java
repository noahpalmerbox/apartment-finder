package webserver;

import java.sql.SQLException;
import java.util.Map;

import com.sun.net.httpserver.HttpExchange;

import admin.*;

public class CreateComplexHandler extends RequestHandler {

	public CreateComplexHandler() {
		super("/createcomplex", "POST");
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		Map<String, String> values = parsePostBody(he.getRequestBody());
		int amenitiesID;
		LaundryType lt = null;
		ParkingType pt = null;
		FlooringType ft = null;
		boolean dishwasher = false;
		boolean airConditioner = false;
		boolean patio = false;
		boolean gym = false;
		boolean pool = false;
		boolean ishouse = false;
		
		switch(values.get("laundry"))
		{
		case "communal":
			lt = LaundryType.COMMUNAL;
			break;
		case "inunit":
			lt = LaundryType.IN_UNIT;
			break;
		}
		switch(values.get("flooring"))
		{
		case "tile":
			ft = FlooringType.TILE;
			break;
		case "hardwood":
			ft = FlooringType.HARDWOOD;
			break;
		}
		switch(values.get("parking"))
		{
		case "garage":
			pt = ParkingType.PERSONAL_GARAGE;
			break;
		case "dedicatedspace":
			pt = ParkingType.DEDICATED_PARKING_SPOT;
			break;
		}
		if(values.get("dishwasher") != null)
		{
			dishwasher = true;
		}
		if(values.get("ishouse") != null)
		{
			ishouse = true;
		}
		if(values.get("airconditioner") != null)
		{
			dishwasher = true;
		}
		if(values.get("gym") != null)
		{
			dishwasher = true;
		}
		if(values.get("pool") != null)
		{
			dishwasher = true;
		}
		try {
			amenitiesID = CreateAmenities.create(lt, pt, ft, dishwasher, airConditioner, patio, gym, pool);
		} catch (SQLException e) {
			e.printStackTrace();
			redirect(he, "/createcomplex.html?message=error");
			return false;
		}
		try {
			CreateComplex.create(values.get("state"), values.get("city"), values.get("streetname"), Integer.parseInt(values.get("streetnumber")), values.containsKey("isclosetocampus"), values.containsKey("petsallowed"), amenitiesID);
		} catch (NumberFormatException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			redirect(he, "/createcomplex.html?message=error");
			return false;
		}
		if(ishouse)
		{
			redirect(he, "/createapartment.html");
		}
		else
		redirect(he, "/admin.htm");
		return true;
	}

}
