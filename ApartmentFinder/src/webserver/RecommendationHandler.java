package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;
import recommendations.Recommendations;

public class RecommendationHandler extends RequestHandler {

	public RecommendationHandler() {
		super("/recommendations", "GET");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		try {
			UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));

			ResultSet recs = new Recommendations(uc.getUserID()).ApartmentRecommendations();
			JSONArray a = new JSONArray();
			while (recs.next()) {

				ResultSet rs = Database.query(
						"SELECT * FROM APARTMENT INNER JOIN COMPLEX ON APARTMENT.ComplexID=COMPLEX.ComplexID WHERE EXISTS (SELECT * FROM AVAILABLE_APARTMENTS WHERE AVAILABLE_APARTMENTS.ApartmentID = APARTMENT.ApartmentID) AND ApartmentID = "
								+ recs.getInt("ApartmentID") + " ORDER BY MonthRent;");
				while (rs.next()) {

					JSONObject o = new JSONObject();
					o.put("id", rs.getInt("ApartmentID"));
					o.put("complexid", rs.getInt("ComplexID"));
					o.put("description", rs.getString("Description"));
					o.put("apartmentnumber", rs.getInt("ApartmentNumber"));
					o.put("deposit", rs.getDouble("Deposit"));
					o.put("monthrent", rs.getDouble("MonthRent"));
					o.put("squarefeet", rs.getInt("SquareFeet"));
					o.put("bedrooms", rs.getInt("Bedrooms"));
					o.put("bathrooms", rs.getInt("Bathrooms"));
					StringBuilder sb = new StringBuilder();
					sb.append(rs.getInt("StreetNumber"));
					sb.append(" ");
					sb.append(rs.getString("StreetName"));
					sb.append(", ");
					sb.append(rs.getString("City"));
					sb.append(", ");
					sb.append(rs.getString("State"));
					o.put("address", sb.toString());

					a.put(o);
				}
			}
			
			OutputStream outputStream = he.getResponseBody();
			he.getResponseHeaders().add("content-type", "application/json");
			he.sendResponseHeaders(200, 0);
			outputStream.write(a.toString().getBytes());
			outputStream.flush();
			outputStream.close();
			he.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}

	}

}
