package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;
import reports.GenerateReports;

public class ReportHandler extends RequestHandler {

	public ReportHandler() {
		super("/getreport", "GET");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		Map<String, String> values = parseGetParams(params);
		if(values.get("type").equals("complex"))
		{
			String startdate = values.get("startdate");
			String enddate = values.get("enddate");
			JSONArray a = new JSONArray();
			try {
				ResultSet rs = GenerateReports.ComplexRevenue(startdate, enddate);
				while(rs.next())
				{
					JSONObject o = new JSONObject();
					o.put("complexid", rs.getInt("ComplexID"));
					o.put("sum", rs.getDouble("Sum"));
					StringBuilder sb = new StringBuilder();
					sb.append(rs.getInt("StreetNumber"));
					sb.append(" ");
					sb.append(rs.getString("StreetName"));
					sb.append(" ");
					sb.append(rs.getString("City"));
					sb.append(", ");
					sb.append(rs.getString("State"));
					o.put("address", sb.toString());
					a.put(o);
				}
				OutputStream outputStream = he.getResponseBody();
				he.sendResponseHeaders(200, 0);
				outputStream.write(a.toString().getBytes());
				outputStream.flush();
				outputStream.close();
				he.close();
				return true;
			} catch (SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				loadErrorPage(500, he);
				return false;
			}
		}
		else if(values.get("type").equals("user"))
		{
			String startdate = values.get("startdate");
			String enddate = values.get("enddate");
			JSONArray a = new JSONArray();
			try {
				ResultSet rs = GenerateReports.UserPaymentsInfo(startdate, enddate);
				while(rs.next())
				{
					JSONObject o = new JSONObject();
					o.put("userid", rs.getInt("UserID"));
					o.put("sum", rs.getDouble("Sum"));
					StringBuilder sb = new StringBuilder();
					o.put("email", rs.getString("Email"));
					o.put("firstname", rs.getString("FirstName"));
					o.put("lastname", rs.getString("LastName"));
					a.put(o);
				}
				OutputStream outputStream = he.getResponseBody();
				he.sendResponseHeaders(200, 0);
				outputStream.write(a.toString().getBytes());
				outputStream.flush();
				outputStream.close();
				he.close();
				return true;
			} catch (SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				loadErrorPage(500, he);
				return false;
			}
		
		}
		else {
			loadErrorPage(404, he);
			return false;
		}
	
	}

}
