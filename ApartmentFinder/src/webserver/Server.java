package webserver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import com.sun.net.httpserver.HttpServer;

import database.Database;

public class Server {
	
	
	
	public static void init()
	{
		//register handlers
		new UserHandler();
		new LoginHandler();
		new CreateUserHandler();
		new ApartmentHandler();
		new CreateComplexHandler();
		new ComplexHandler();
		new CreateApartmentHandler();
		new RentalRequestHandler();
		new RecommendationHandler();
		new ApartmentViewHandler();
		new AdminRentalRequestHandler();
		new RentalRequestStatusHandler();
		new UserContractInfoHandler();
		new RentalInfoHandler();
		new GetBillsHandler();
		new PoPalHandler();
		new AdminGetUserData();
		new ContractHandler();
		new CancelRequestHandler();
		new AdminCancelRequestHandler();
		new CancelRequestStatusHandler();
		new SearchResultsHandler();
		new LateFeeHandler();
		new GetReviewHandler();
		new CreateReviewHandler();
		new ReportHandler();
		PagePerm.init();
		HttpServer server = null;
		try {
			server = HttpServer.create(new InetSocketAddress(8001), 20);
		} catch (IOException e) {
				System.err.println("Failed to create HttpServer:");
				e.printStackTrace();
				System.exit(-1);
		}
		
		ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor)Executors.newFixedThreadPool(1);
		
		server.createContext("/", new CustomHttpHandler());
		server.setExecutor(threadPoolExecutor);
		server.start();
		System.out.println("Server started on port 8001");
	}
}
