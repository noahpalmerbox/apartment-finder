package webserver;

import java.sql.SQLException;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class LateFeeHandler extends RequestHandler {

	public LateFeeHandler() {
		super("/assesslatefees", "GET");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		try {
			Database.execute("UPDATE BILL INNER JOIN BILL_DETAILS ON BILL.BillID = BILL_DETAILS.BillID INNER JOIN RENTAL ON BILL.RentalID = RENTAL.RentalID SET LateFeeAssessed=1, AmountDue=AmountDue+LateFee WHERE IsLate=1 AND LateFeeAssessed=1;");
			redirect(he, "/feesuccess.html");
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
