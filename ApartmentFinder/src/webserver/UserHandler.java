package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;

import com.sun.net.httpserver.HttpExchange;

import database.Database;

public class UserHandler extends RequestHandler {

	public UserHandler() {
		super("/user", "GET");

	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		UserCookie uc = UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
		
		
		OutputStream outputStream = he.getResponseBody();
		try {
			he.sendResponseHeaders(200, 0);
			
			if(params.equals("data=isloggedin"))
			{
				if(uc != null)
				{
					outputStream.write("true".getBytes());
					outputStream.flush();
					outputStream.close();
					he.close();
					return true;
				}
				else
				{
					outputStream.write("false".getBytes());
					outputStream.flush();
					outputStream.close();
					he.close();
					return true;
				}
			}
			
			ResultSet rs = Database
					.query("SELECT FirstName, LastName, Email, isAdmin FROM USER WHERE UserID=" + uc.getUserID() + ";");
			rs.next();
			if (params.equals("data=firstname")) {
				outputStream.write(rs.getString("FirstName").getBytes());
			} else if (params.equals("data=lastname")) {
				outputStream.write(rs.getString("LastName").getBytes());
			} else if (params.equals("data=email")) {
				outputStream.write(rs.getString("Email").getBytes());
			} else if (params.equals("data=isadmin")) {
				outputStream.write((rs.getBoolean("isAdmin") ? "true" : "false").getBytes());

			}
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("Boom");
			loadErrorPage(500, he);
			return false;
		}
		try {
			outputStream.flush();
			outputStream.close();
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//System.out.println("Boom2");
			loadErrorPage(500, he);
			return false;
		}catch(NullPointerException e)
		{
			e.printStackTrace();
			//System.out.println("Boom2");
			loadErrorPage(404, he);
		}
		he.close();

		return true;
	}

}
