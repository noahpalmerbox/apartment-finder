package webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;

import database.Database;
import recommendations.Recommendations;
import search.Search;
import search.SearchQuery;

public class SearchResultsHandler extends RequestHandler {

	public SearchResultsHandler() {
		super("/searchresults", "GET");
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean handle(HttpExchange he, String params) {
		try {
			// UserCookie uc =
			// UserCookie.getCookie(he.getRequestHeaders().getFirst("Cookie"));
		
			Map<String, String> values = parseGetParams(params);
		

			SearchQuery sq = new SearchQuery();
			if (!values.get("minsqft").equals(""))
				sq.minSqrFeet = Integer.parseInt(values.get("minsqft"));
			if (!values.get("maxsqft").equals(""))
				sq.maxSqrFeet = Integer.parseInt(values.get("maxsqft"));
			if (!values.get("maxprice").equals(""))
				sq.maxRent = Integer.parseInt(values.get("maxprice"));
			if (!values.get("minprice").equals(""))
				sq.minRent = Integer.parseInt(values.get("minprice"));
			sq.bedrooms = Integer.parseInt(values.get("room"));
			sq.bathrooms = Integer.parseInt(values.get("bath"));
			if (!values.get("pool").equals("no")) {
				sq.hasPool = Boolean.parseBoolean(values.get("pool")) ? 1 : 0;
			}
			if (!values.get("laundry").equals("no")) {
				switch (values.get("laundry")) {
				case "communal":
					sq.laundryType = "\'Communal\'";
					break;
				case "inunit":
					sq.laundryType = "\'In Unit\'";
					break;
				}
			}
			if (!values.get("airconditioning").equals("no")) {
				sq.hasAirConditioner = Boolean.parseBoolean(values.get("airconditioning")) ? 1 : 0;
			}
			if (!values.get("pets").equals("no")) {
				sq.petsAllowed = Boolean.parseBoolean(values.get("pets")) ? 1 : 0;
			}
			if (!values.get("parking").equals("no")) {
				switch (values.get("parking")) {
				case "dedicatedspot":
					sq.parkingType = "\'Dedicated Parking Spot\'";
					break;
				case "parkinggarage":
					sq.parkingType = "\'Personal Garage\'";
					break;
				}
			}
			if (!values.get("flooring").equals("no")) {
				switch (values.get("flooring")) {
				case "hardwood":
					sq.flooringType = "\'Hardwood\'";
					break;
				case "tile":
					sq.flooringType = "\'Tile\'";
					break;
				}
			}
			if (!values.get("dish").equals("no")) {
				sq.hasDishwasher = Boolean.parseBoolean(values.get("dish")) ? 1 : 0;
			}
			if (!values.get("patio").equals("no")) {
				sq.hasPatio = Boolean.parseBoolean(values.get("patio")) ? 1 : 0;
			}
			if (!values.get("gym").equals("no")) {
				sq.hasGym = Boolean.parseBoolean(values.get("gym")) ? 1 : 0;
			}

			ResultSet search = Search.search(sq);
			JSONArray a = new JSONArray();
			while (search.next()) {

				ResultSet rs = Database.query(
						"SELECT * FROM APARTMENT INNER JOIN COMPLEX ON APARTMENT.ComplexID=COMPLEX.ComplexID WHERE EXISTS (SELECT * FROM AVAILABLE_APARTMENTS WHERE AVAILABLE_APARTMENTS.ApartmentID = APARTMENT.ApartmentID) AND ApartmentID = "
								+ search.getInt("ApartmentID") + " ORDER BY MonthRent;");
				while (rs.next()) {

					JSONObject o = new JSONObject();
					o.put("id", rs.getInt("ApartmentID"));
					o.put("complexid", rs.getInt("ComplexID"));
					o.put("description", rs.getString("Description"));
					o.put("apartmentnumber", rs.getInt("ApartmentNumber"));
					o.put("deposit", rs.getDouble("Deposit"));
					o.put("monthrent", rs.getDouble("MonthRent"));
					o.put("squarefeet", rs.getInt("SquareFeet"));
					o.put("bedrooms", rs.getInt("Bedrooms"));
					o.put("bathrooms", rs.getInt("Bathrooms"));
					StringBuilder sb = new StringBuilder();
					sb.append(rs.getInt("StreetNumber"));
					sb.append(" ");
					sb.append(rs.getString("StreetName"));
					sb.append(", ");
					sb.append(rs.getString("City"));
					sb.append(", ");
					sb.append(rs.getString("State"));
					o.put("address", sb.toString());

					a.put(o);
				}
			}

			OutputStream outputStream = he.getResponseBody();
			he.getResponseHeaders().add("content-type", "application/json");
			he.sendResponseHeaders(200, 0);
			outputStream.write(a.toString().getBytes());
			outputStream.flush();
			outputStream.close();
			he.close();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadErrorPage(500, he);
			return false;
		}

	}

}
