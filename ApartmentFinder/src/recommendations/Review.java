package recommendations;

import java.sql.SQLException;

import database.Database;

public class Review {
	String review;
	int rating;
	int userID;
	int rentalID;
	
	public Review(String review, int rating, int userId, int rentalId) throws SQLException {
		this.review = review;
		this.rating = rating;
		this.userID = userId;
		this.rentalID = rentalId;
		SubmitReview();
	}
	
	public void SubmitReview() throws SQLException {
		Database.execute("INSERT INTO REVIEW(UserID, RentalID, Rating, Review) "
				+ "VALUES( " + userID + ", " + rentalID + ", " + rating + ", " + review + ")");
	}
}
