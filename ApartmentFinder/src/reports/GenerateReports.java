package reports;

import java.sql.ResultSet;
import java.sql.SQLException;

import database.Database;

public class GenerateReports {

    public static ResultSet UserPaymentsInfo (String start, String end) throws SQLException {
            ResultSet rs = Database.query("SELECT PAYMENT.UserID, SUM(Amount) as 'Sum', Email, FirstName, LastName  \n"
            		+ "                    FROM PAYMENT INNER JOIN USER ON PAYMENT.UserID = USER.UserID WHERE PAYMENT.PaymentDate BETWEEN \'"+start+"\'  AND \'"+end+"\'\n"
            		+ "                    Group By PAYMENT.UserID,Email,FirstName,LastName;");
        return rs;


}
    public static ResultSet ComplexRevenue(String start, String end) throws SQLException{
        ResultSet rs = Database.query("Select COMPLEX.ComplexID, sum(AmountDue) AS Sum, State, City, StreetName, StreetNumber\n" +
                "From RENTAL as R Join BILL As B\n" + 
                "    on R.RentalID = B.RentalID\n" + 
                "    JOIN APARTMENT as A\n" + 
                "        ON A.ApartmentID = R.ApartmentID\n" +
                " INNER JOIN COMPLEX ON A.ComplexID = COMPLEX.ComplexID" +
                " WHERE B.BillDue BETWEEN \'"+start+
                "\' AND \'" + end + "\'" +
                "group by COMPLEX.ComplexID\n" + 
                "order by COMPLEX.ComplexID");
    return rs;
    }
}

