package admin;

import java.sql.SQLException;

import database.Database;

public class CancellationRequest {
	
	public CancellationRequest(boolean accept, int rentalID) throws SQLException {
		Database.execute("UPDATE RENTAL SET RequestDecision = null,"
				+ " AdminResponse = null WHERE RentalID = " + rentalID);
		if(accept) {
			Database.execute("UPDATE RENTAL"
				+ " SET RequestDecision = " + accept + ","
				+ " AdminResponse = 'We accept your request. Communicate with the  leasing office for more details.'" 
				+ " WHERE RentalID = " + rentalID
				);
			
			Database.execute("DELETE FROM BILL "
					+ "WHERE BillDue > curdate() and RentalID = " + rentalID);
		}
		else {
			Database.execute("UPDATE RENTAL"
					+ " SET RequestDecision = " + accept + ","
					+ " AdminResponse = 'We have denied your request. Communicate with the leasing office for more details.'"
					+ " WHERE RentalID = " + rentalID
					);
		}
	}
}
