package database;
import java.sql.*;

public class Database {
	private static Connection con;
	//init the Database connection
	public static void init() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			//TODO Remove the Username and password from the code.
			con = DriverManager.getConnection("jdbc:mysql://ec2-34-226-70-67.compute-1.amazonaws.com:3306/ApartmentFinder?user=bot&password=KNapmZrNVtU9k39d&characterEncoding=latin1&serverTimezone=UTC");
		

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	
	//Use query for SQL commands that return results e.g. SELECT
	public static ResultSet query(String s) throws SQLException
	{
		Statement stmt = con.createStatement();
		return stmt.executeQuery(s);
	}
	
	
	//Use execute for commands that don't return results e.g. INSERT. The boolean is for success
	public static boolean execute(String s) throws SQLException
	{
		Statement stmt = con.createStatement();
		return stmt.execute(s);
		
	}
	
	public static String escapeString(String s)
	{
		StringBuilder sb = new StringBuilder(s);
		int index = 0;
		while(sb.indexOf("\'", index) != -1)
		{
		sb.insert(sb.indexOf("\'", index), "\\");
		index = sb.indexOf("\'", index)+1;
		}
		index = 0;
		while(sb.indexOf("\"", index) != -1)
		{
		sb.insert(sb.indexOf("\"", index), "\\");
		index = sb.indexOf("\"", index)+1;
		}
		return sb.toString();
		
		
	}
	
	public static void startTransaction(){
		try {
			Database.execute("START TRANSACTION;");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void commitTransaction()
	{
		try {
			Database.execute("COMMIT;");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void rollbackTransaction()
	{
		try {
			Database.execute("ROLLBACK;");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	//closes the database connection
	public static void close()
	{
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
