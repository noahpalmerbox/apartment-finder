package login;

public class LoginToken {

	public int userID;
	public boolean accountExists;
	public boolean loginCorrect;
	
	public LoginToken(int userID, boolean accountExists, boolean loginCorrect)
	{
		this.userID = userID;
		this.accountExists = accountExists;
		this.loginCorrect = loginCorrect;
	}
	
}
