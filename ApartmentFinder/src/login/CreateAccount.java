package login;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;

import database.Database;

public class CreateAccount {
	private String email;
	private String password;
	private String firstName;
	private String lastName;
	private String passwordHash;
	private String passwordSalt;
	
	public CreateAccount(String email, String password, String first, String last) throws NoSuchAlgorithmException{
		this.email = email;
		this.password = password;
		this.firstName = first;
		this.lastName = last;
		PasswordSecurity ps = new PasswordSecurity(password);
		this.passwordSalt = Base64.getEncoder().encodeToString(ps.getSalt());
		this.passwordHash =  ps.hashPassword(Base64.getDecoder().decode(passwordSalt));
	
	}

	private boolean VerifyEmail() {
		
		try {
			ResultSet acceptEmail = Database.query("SELECT UserID FROM USER WHERE Email = \'" + email + "\';");
			if(acceptEmail.next()) {
				return false;
			}
			else {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		return false;
		
	}

	public boolean AccountCreated() throws SQLException {
	
		if(VerifyEmail()) {
		
			
				
				Database.execute("INSERT INTO USER (LastName, FirstName, Email, PasswordHash, PasswordSalt) VALUES (\'" + Database.escapeString(lastName) + "\', \'" + Database.escapeString(firstName) + "\', \'" + Database.escapeString(email) + "\', \'" + passwordHash + "\', \'" + passwordSalt + "\');");
				return true;
		
		}
		return false;
		
	}
}//end CreateAccount class
